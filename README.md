# Users

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

###Plan/WIP:

1. App

	1.1 Components

		1.1.1 List v

			1.1.1.1 List Item v

 		1.1.2 View v

			1.1.2.1 Markup v

			1.1.2.2 Controller v

		1.1.3 Edit/New

			1.1.3.1 Controller v

			1.1.3.2 Markup v

				1.1.3.2.1 Basic v

				1.1.3.2.2 Extend with templates v

			1.1.3.3 Display errors

			1.1.3.4 Additional validators

	1.2 Router v

	1.3 Service v

	1.4 Styles +/-

2. Tests +/-

3. Intrnalization

4. Own BackEnd

