import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input }                 from '@angular/core';

import { ListComponent } from './list.component';
import { UserService } from '../../user.service';

describe('ListComponent', () => {
    let component: ListComponent;
    let fixture: ComponentFixture<ListComponent>;

    @Component({ selector: 'user-edit', template: '' })
    class EditStubComponent {}

    @Component({ selector: '[appListItem]', template: ''})
    class ListItemStubComponent {
        @Input() user;
        @Input() index;
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ListComponent, EditStubComponent, ListItemStubComponent ],
            providers: [
                { provide: UserService, values: { } }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
