import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Injectable, Directive, Input } from '@angular/core';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AppRoutingModule } from '../../..//app-routing.module';
import { ListItemComponent } from './list-item.component';

describe('ListItemComponent', () => {
    let component: ListItemComponent;
    let fixture: ComponentFixture<ListItemComponent>;

    @Component({ selector: 'user-edit', template: '' })
    class EditStubComponent {}

    @Directive({
        selector: '[routerLink]',
        host: {
            '(click)': 'onClick()'
        }
    })
    class RouterLinkStubDirective {
        @Input('routerLink') linkParams: any;
        navigatedTo: any = null;
      
        onClick() {
            this.navigatedTo = this.linkParams;
        }
    }

    @Injectable()
    class RouterStub {
        navigate(commands: any[], extras?: NavigationExtras) { }
    }

    @Injectable()
    class ActivatedRouteStub {
    
        // ActivatedRoute.params is Observable
        private subject = new BehaviorSubject(this.testParams);
        params = this.subject.asObservable();
        
        // Test parameters
        private _testParams: {};
        get testParams() { return this._testParams; }
        set testParams(params: {}) {
            this._testParams = params;
            this.subject.next(params);
        }
        
        // ActivatedRoute.snapshot.params
        get snapshot() {
            return { params: this.testParams };
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ListItemComponent, EditStubComponent, RouterLinkStubDirective ],
            providers: [
                { provide: Router, useClass: RouterStub },
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListItemComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
