import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../user';

@Component({
    selector: '[appListItem]',
    templateUrl: './list-item.component.html',
    styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
    @Input() index: number;
    @Input()  user: User;

    constructor() { }

    ngOnInit() {
    }

}
