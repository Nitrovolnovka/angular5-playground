import { Component, OnInit } from '@angular/core';
import { USER_PROPERTIES_MAP } from '../../../constants';
import { User } from '../../user';
import { UserService } from '../../user.service';

@Component({
    selector: 'user-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    users: User[] = [];

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.getUsers();
    }

    getUsers(): void {
        this.userService.getUsers()
            .subscribe(users => this.users = users);
    }

};
