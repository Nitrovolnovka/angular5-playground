import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Directive, Input, Injectable } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ViewComponent } from './view.component';
import { UserService } from '../../user.service';

describe('ViewComponent', () => {
    let component: ViewComponent;
    let fixture: ComponentFixture<ViewComponent>;

    @Directive({
        selector: '[routerLink]',
        host: {
            '(click)': 'onClick()'
        }
    })
    class RouterLinkStubDirective {
        @Input('routerLink') linkParams: any;
        navigatedTo: any = null;
      
        onClick() {
            this.navigatedTo = this.linkParams;
        }
    }

    @Component({ selector: 'app-user-view-block', template: ''})
    class BlockStubComponent {
        @Input() data;
        @Input() blockName;
        @Input() labels;
    }

    @Injectable()
    class ActivatedRouteStub {
    
        // ActivatedRoute.params is Observable
        private subject = new BehaviorSubject(this.testParams);
        params = this.subject.asObservable();
        
        // Test parameters
        private _testParams: {};
        get testParams() { return this._testParams; }
        set testParams(params: {}) {
            this._testParams = params;
            this.subject.next(params);
        }
        
        // ActivatedRoute.snapshot.params
        get snapshot() {
            return { params: this.testParams };
        }
    }

    @Injectable()
    class RouterStub {
        navigate(commands: any[], extras?: NavigationExtras) { }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ViewComponent, BlockStubComponent, RouterLinkStubDirective ],
            providers: [
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: Router, useClass: RouterStub },
                { provide: UserService, values: { } }
            ]

        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
