import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { User } from '../../user';
import { USER_PROPERTIES_MAP } from '../../../constants';
import { UserService } from '../../user.service';

@Component({
    selector: 'user-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
    user: User;
    labels = USER_PROPERTIES_MAP;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService
    ) { }
    
    ngOnInit() {
        this.getUser();
    }

    getUser() {
        const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.userService.getUser(id).subscribe(user => this.user = user);    
    }

}
