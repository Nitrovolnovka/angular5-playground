import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-user-view-row',
    templateUrl: './row.component.html',
    styleUrls: ['./row.component.css']
})
export class RowComponent implements OnInit {
    @Input() label: string;
    @Input() value: any;

    constructor() { }

    ngOnInit() {}

}
