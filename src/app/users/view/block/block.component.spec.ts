import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { BlockComponent } from './block.component';

describe('BlockComponent', () => {
    let component: BlockComponent;
    let fixture: ComponentFixture<BlockComponent>;

    @Component({ selector: 'app-user-view-row', template: ''})
    class RowStubComponent {
        @Input() label;
        @Input() value;
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ BlockComponent, RowStubComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BlockComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
