import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-user-view-block',
    templateUrl: './block.component.html',
    styleUrls: ['./block.component.css']
})
export class BlockComponent implements OnInit {
    @Input() blockName: string;
    @Input() labels: object;
    @Input() data: object;

    constructor() { }

    ngOnInit() {
    }

    isObject(item) {
        return typeof item === 'object';
    }

    getKeys(obj) {
        return Object.keys(obj);
    }
}
