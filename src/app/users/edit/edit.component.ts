import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import getIn from 'lodash.get';
import each from 'lodash.foreach';
import omit from 'lodash.omit';

import { USER_PROPERTIES_MAP, USER_HINTS } from '../../../constants';
import { User } from '../../user';
import { UserService } from '../../user.service';

@Component({
    selector: 'user-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
    userForm: FormGroup;
    labels = USER_PROPERTIES_MAP;
    hints = USER_HINTS;
    newPath = 'new';
    editPath = 'edit';
    userId: number;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
    ) { }

    ngOnInit() {
        this.createUserForm();
        if (this.isEditForm())
            this.initEditForm();
        else if (!this.isNewForm()) {
            this.router.navigateByUrl('404');
        }
        console.log('>>>this.labels', this.labels);
    }

    isNewForm() {
        const { path } = this.route.snapshot.url[0];
        return path == this.newPath;
    }

    isEditForm() {
        const { path } = this.route.snapshot.url[0];
        return path == this.editPath;
    }

    initEditForm() {
        this.getUser().subscribe(user => {
            this.userId = user.id;
            this.userForm.setValue(omit(user, 'id'));
        });
    }

    getUser() {
        const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        return this.userService.getUser(id)
    }

    createUserForm() {
        this.userForm = this.createGroupForm(this.labels);
    }

    createGroupForm(obj) {
        const _fields = {};
        each(obj, (item: any, key: string) => {
            if (this.isObject(item))
                _fields[key] = this.createGroupForm(item);
            else
                _fields[key] = ['', Validators.required];
        });
        return this.fb.group(_fields);
    }

    saveForm() {
        const { value, valid } = this.userForm;
        if (valid)
            if (this.isNewForm())
                this.userService.addUser(value).subscribe(
                    response => this.userSaved(response),
                    error => this.userSaveError(error)
                );
            else 
                this.userService.updateUser({
                    ...value,
                    id: this.userId
                }).subscribe(
                    response => this.userSaved(response),
                    error => this.userSaveError(error)
                );;
    }

    userSaved(response) {
        alert('saved successfully');
        this.router.navigate(['/list']);
    }

    userSaveError(error) {
        alert(error.message);
        console.error(error);
    }

    isObject(item) {
        return typeof item === 'object';
    }
}
