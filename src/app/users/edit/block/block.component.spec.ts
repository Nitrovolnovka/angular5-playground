import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { BlockFormComponent } from './block.component';

describe('BlockComponent', () => {
    let component: BlockFormComponent;
    let fixture: ComponentFixture<BlockFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ BlockFormComponent ],
            providers: [ FormBuilder ],
            imports: [ ReactiveFormsModule ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(BlockFormComponent);
            component = fixture.componentInstance;
        });
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
