import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import getIn from 'lodash.get';
import each from 'lodash.foreach';

@Component({
    selector: 'app-user-form-block',
    templateUrl: './block.component.html',
    styleUrls: ['./block.component.scss']
})
export class BlockFormComponent implements OnInit {
    @Input() blockName: string;
    @Input() labels: object;
    @Input() hints: object;
    @Input() values: object;
    @Input() form: FormGroup;
    @Input() isTop: boolean;
    @Output() onSubmit = new EventEmitter<boolean>();

    constructor(
        private fb: FormBuilder
    ) { }

    ngOnInit() {
    }

    isObject(item) {
        return typeof item === 'object';
    }

    getKeys(obj) {
        return Object.keys(obj);
    }
}
