import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../..//app-routing.module';
import { Component, Input, Directive, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NavigationExtras, Router, ActivatedRoute } from '@angular/router';

import { EditComponent } from './edit.component';
import { UserService } from '../../user.service';

describe('EditComponent', () => {
    let component: EditComponent;
    let fixture: ComponentFixture<EditComponent>;

    @Component({ selector: 'app-user-form-block', template: '' })
    class BlockStubComponent {
        @Input() blockName: string;
        @Input() labels: object;
        @Input() hints: object;
        @Input() values: object;
        @Input() form: FormGroup;
        @Input() isTop: boolean;
    }

    @Component({ selector: 'app-page-not-found', template: '' })
    class Page404StubComponent { }

    @Component({ selector: 'router-outlet', template: '' })
    class RouterOutletStubComponent { }    

    @Injectable()
    class ActivatedRouteStub {
    
        // ActivatedRoute.params is Observable
        private subject = new BehaviorSubject(this.testParams);
        params = this.subject.asObservable();
        
        // Test parameters
        private _testParams: {};
        get testParams() { return this._testParams; }
        set testParams(params: {}) {
            this._testParams = params;
            this.subject.next(params);
        }
        
        // ActivatedRoute.snapshot.params
        get snapshot() {
            return { params: this.testParams };
        }
    }

    @Directive({
        selector: '[routerLink]',
        host: {
            '(click)': 'onClick()'
        }
    })
    class RouterLinkStubDirective {
        @Input('routerLink') linkParams: any;
        navigatedTo: any = null;
      
        onClick() {
            this.navigatedTo = this.linkParams;
        }
    }

    @Injectable()
    class RouterStub {
        navigate(commands: any[], extras?: NavigationExtras) { }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditComponent,
                Page404StubComponent,
                BlockStubComponent,
                RouterOutletStubComponent,
                RouterLinkStubDirective
            ],
            imports: [ ReactiveFormsModule ],
            providers: [
                FormBuilder,
                { provide: ActivatedRoute, useClass: ActivatedRouteStub },
                { provide: Router, useClass: RouterStub },
                { provide: UserService, values: {} }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
