import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesComponent } from './messages.component';
import { MessageService } from '../message.service';

describe('MessagesComponent', () => {
    let component: MessagesComponent;
    let fixture: ComponentFixture<MessagesComponent>;
    const messagesServiceStub = ['msg1', 'msg2'];

    class MessageServiceSpy {
        messages: string[] = [];
        
        add = jasmine.createSpy('add').and.callFake(() => { });
        clear = jasmine.createSpy('clear').and.callFake(() => { });
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ MessagesComponent ],
            providers:    [ { provide: MessageService, useValue: messagesServiceStub } ]
        })
        .overrideComponent(MessagesComponent, {
            set: {
                providers: [
                    { provide: MessageService, useClass: MessageServiceSpy }
                ]
            }
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MessagesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
