import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './users/list/list.component';
import { ViewComponent } from './users/view/view.component';
import { EditComponent } from './users/edit/edit.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
    { path: 'list', component: ListComponent },
    { path: '', redirectTo: '/list', pathMatch: 'full' },
    { path: 'view/:id', component: ViewComponent },
    { path: 'new', component: EditComponent },
    { path: 'edit/:id', component: EditComponent },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }

