import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { User } from './user';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {
    private baseUrl: string = 'http://jsonplaceholder.typicode.com';
    private usersUrl: string = '/users';
    private userUrl: string = '/users/<id>';

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) { }

    getUsers(): Observable<User[]> {
        const url = this.getUrl(this.usersUrl);
        this.log('UserService: fetching users from ' + url);
        return this.http.get<User[]>(url)
            .pipe(
                tap(users => this.log('fetched users: ' + users.length)),
                catchError(this.handleError('getUsers', []))
            );
    }

    /** GET user by id. Will 404 if id not found */
    getUser(id: number): Observable<User> {
        const url = this.getUrl(this.userUrl, id);
        return this.http.get<User>(url)
            .pipe(
                tap(_ => this.log(`fetched user id=${id}`)),
                catchError(this.handleError<User>(`getUser id=${id}`))
            );
    }

    /** PUT: update the user on the server */
    updateUser (user: User): Observable<any> {
        const url = this.getUrl(this.userUrl, user.id);
        return this.http.put(url, user, httpOptions)
            .pipe(
                tap(_ => this.log(`updated user id=${user.id}`)),
                catchError(this.handleError<any>('updateUser'))
            );
    }

    /** POST: add a new user to the server */
    addUser (user: User): Observable<any> {
        const url = this.getUrl(this.usersUrl);
        return this.http.post(url, user, httpOptions)
            .pipe(
                tap((user: User) => this.log(`added user with name=${user.name}`)),
                catchError(this.handleError<User>('addUser'))
            );
    }

    /** formats url with base domain url */
    getUrl(path: string, id?: number): string {
        if (id)
            path = path.replace('<id>', id.toString());
        return this.baseUrl + path;
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
        
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
        
            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
        
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    private log(message: string): void {
        this.messageService.add('HeroService: ' + message);        
    }

}
