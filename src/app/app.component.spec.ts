import { TestBed, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { Component }                 from '@angular/core';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
    @Component({selector: 'app-users', template: ''})
    class UsersStubComponent {}

    @Component({selector: 'app-messages', template: ''})
    class MessagesStubComponent {}
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                UsersStubComponent,
                MessagesStubComponent
            ],
        }).compileComponents();
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it(`should have as title 'Users Management List'`, async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('Users Management List');
    }));
    it('should render title in a h3 tag', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h3').textContent).toContain('Users Management List');
    }));
});
