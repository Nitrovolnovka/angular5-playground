import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ListComponent } from './users/list/list.component';
import { ViewComponent } from './users/view/view.component';
import { EditComponent } from './users/edit/edit.component';
import { AppRoutingModule } from './/app-routing.module';
import { UserService } from './user.service';
import { MessageService } from './message.service';
import { MessagesComponent } from './messages/messages.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ListItemComponent } from './users/list/list-item/list-item.component';
import { RowComponent } from './users/view/row/row.component';
import { BlockComponent } from './users/view/block/block.component';
import { BlockFormComponent } from './users/edit/block/block.component';


@NgModule({
    declarations: [
        AppComponent,
        UsersComponent,
        ListComponent,
        ViewComponent,
        EditComponent,
        MessagesComponent,
        PageNotFoundComponent,
        ListItemComponent,
        RowComponent,
        BlockComponent,
        BlockFormComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [ UserService, MessageService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
