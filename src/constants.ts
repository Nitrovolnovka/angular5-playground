export const USER_PROPERTIES_MAP = {
    name: 'Full Name',
    username: 'Username',
    email: 'Email',
    address: {
        street: 'Street',
        suite: 'Suite',
        city: 'City',
        zipcode: 'Zip Code',
        geo: {
            lat: 'Latitude',
            lng: 'Longitude'
        }
    },
    phone: 'Phone',
    website: 'Website',
    company: {
        name: 'Company Name',
        catchPhrase: 'Company Slogan',
        bs: 'Company Tags'
    }
};

export const USER_HINTS = {
    name: 'User full name...',
    username: 'User login id...',
    email: '...@...',
    address: {
        street: 'Street name...',
        suite: 'Suite number...',
        city: 'City name...',
        zipcode: 'XXXXX...',
        geo: {
            lat: 'xx.xxxx...',
            lng: 'yy.yyyy...'
        }
    },
    phone: '+...',
    website: 'xxxxx.xx...',
    company: {
        name: 'Company name...',
        catchPhrase: 'Slogan...',
        bs: 'tag1 tag2 ...'
    }
}
